package com.test.ClientChat;

import android.util.Log;

import java.io.Console;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class WriteThread extends Thread {
    private PrintWriter writer;
    private Socket socket;
    private ChatClientThread client;

    public WriteThread(Socket socket, ChatClientThread client) {
        this.socket = socket;
        this.client = client;

        try {
            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);
        } catch (IOException ex) {
            System.out.println("Error getting output stream: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void run() {

      //  Console console = System.console();

      // String userName = console.readLine("\nEnter your name: ");
       String userName = "test1";
        client.setUserName(userName);
        writer.println(userName);

        String text;
        int ciclo =0;
        do {
           // text = console.readLine("[" + userName + "]: ");
            if(ciclo >= 10  ){
                text = "bye";
            }else{
                text = "hello word";
            }

            writer.println(text);
            Log.i("ClientChat","send message"+text);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            ciclo++;


        } while (!text.equals("bye"));

        try {
            socket.close();
            Log.i("ClientChat","socket.close()"+text);
        } catch (IOException ex) {

            System.out.println("Error writing to server: " + ex.getMessage());
        }
    }
}
